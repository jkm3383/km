package com.greedy.level03.hard;

public class RandomMaker {
	
	/*
	 * "앞면" 또는 "뒷면"이라는 문자열 중에 하나를 랜덤하게 반환하는 메소드를 작성하시오.
	 * 
	 * 삼항연산자를 사용해서 풀기
	 */
	public static String tossCoin() {
		int random = (int)(Math.random() * 2);
		
		String str = (random == 0) ? "앞면" : "뒷면";
		System.out.println(str);
		return "";
	}
	
	/*
	 * "가위", "바위", "보"이라는 문자열 중에 하나를 랜덤하게 반환하는 메소드를 작성하시오.
	 * 
	 * 삼항연산자를 사용해서 풀기
	 */
	public static String rockPaperScissors() {
		int random = (int)(Math.random() * 3);
		
		String str = (random == 0) ? "가위" :(random == 1) ? "바위" : "보";
		System.out.println(str);
		return "";
	}

	/*
	 * 매개변수로 넘어온 숫자 갯수만큼의 임의의 대문자 영어 알파벳들을
	 * String으로 반환하는 메소드
	 * 
	 * ex)
	 * "XAHEIJGHBI"와 같은 문자열이 반환 됨 
	 */
	public static String randomUpperAlphabet(int length) {
		
		String result = "";
		for(int i = 0; i < length; i++) {
			int random1 = (int)(Math.random() * 26) + 65;
			char random2 = (char)random1;
			String random3 = String.valueOf(random2);
			result += random3;
		}
		return result;
	}
	
	/*
	 * 위의 randomUpperAlphabet으로 발생하는 대문자 영단어를
	 * 소문자로 바꿔서 String으로 반환하는 메소드
	 * 
	 * ex)
	 * "xaheijghbi"와 같은 문자열이 반환 됨 
	 */
	public static String toLowerAlphabet(int length) {
		String str = RandomMaker.randomUpperAlphabet(length);
		System.out.println(str);
		
		String str2 = str.toLowerCase();
		System.out.println(str2);
		return "";
	}
}
