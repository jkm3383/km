package com.greedy.section01.generic;

public class ObjectTest {

	private  Object value;

	public ObjectTest() {
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public ObjectTest(Object value) {
		this.value = value;
	}
}
