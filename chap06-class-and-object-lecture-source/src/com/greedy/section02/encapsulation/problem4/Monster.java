package com.greedy.section02.encapsulation.problem4;

public class Monster {
	private String kinds;	
	private int hp;
	
	/* setter (값을 넣기 위한 메소드) */
	public void setInfo(String info) {
		this.kinds = info;
	}
	
	public void setHp(int hp) {
		if(hp >= 0) this.hp = hp;
	}
	
	/* getter (값을 뽑아내기 위한 메소드) */
	public String getInfo() {
		return this.kinds;
	}
	
	public int getHp() {
		return this.hp;
	}
}
